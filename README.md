# Kar-Archive-Format

## Building:
``bash build.sh``

## Making an archive:
``kar -i in_folder -o archive.kar``

## Extracting an archive:
``kar -i archive.kar -e``

## Information:
The input folder holds files you want to put
into an archive.
