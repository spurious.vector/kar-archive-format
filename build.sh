fail() {
  cd ../
  rm -rf bfiles
  rm -rf bin
}

mkdir bin
mkdir bfiles
cd bfiles
cmake ..
make        || fail
cd ../
rm -rf bfiles
