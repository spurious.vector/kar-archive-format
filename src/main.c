#include <stdio.h>
#include <argp.h>
#include <stdint.h>
#include <kararchive.h>

static uint8_t input_specified = 0;
static uint8_t output_specified = 0;
static uint8_t extract_file = 0;
static const char* input_file = NULL;
static const char* output_file = NULL;

static error_t parse_opt(int key, char* arg, struct argp_state* state) {
  switch (key) {
    case 'i':
      input_specified = 1;
      input_file = arg;
      break;
    case 'o':
      output_specified = 1;
      output_file = arg;
      break;
    case 'e':
      extract_file = 1;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

static struct argp_option options[] = {
  {"in", 'i', "INPUT FOLDER", 0, "Input folder"},
  {"out", 'o', "OUTPUT FOLDER", 0, "Output archive"},
  {"extract", 'e', 0, 0, "Set if we want to extract the file"},
  {0}
};

static struct argp argp = {options, parse_opt, NULL, NULL};

int main(int argc, char** argv) {
  argp_parse(&argp, argc, argv, 0, 0, NULL);

  if (!(input_specified)) {
    printf("ERROR: Expected input folder, use --help\n");
    return 1;
  }

  if (extract_file) {
    extract_archive(input_file);
    return 0;
  }

  if (!(output_specified)) {
    printf("ERROR: Expected output archive path.\n");
    return 1;
  }

  archive_append(output_file, input_file);
  return 0;
}
