#include <archive.h>
#include <stdio.h>
#include <stddef.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define FILE_END_SIG "KAR FILE END"

static FILE* output_file = NULL;
static const char MAGIC[6] = {'K', 'R', 10, 14, 5, 0};

static uint64_t hex2int(const char* hex, size_t len) {
    uint64_t val = 0;

    while (*hex == ' ') { ++hex; }
    if (*hex == '0' && *(hex + 1) == 'x')
        hex += 2;

    for (int i = 0; i < len; ++i) {
        // get current character then increment
        uint8_t byte = *hex++;
        // transform hex character to the 4bit equivalent number, using the ascii table indexes
        if (byte >= '0' && byte <= '9') byte = byte - '0';
        else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
        else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;
        // shift 4 to make space for new digit, and add the 4 bits of the new digit
        val = (val << 4) | (byte & 0xF);
    }
    return val;
}

static char* get_path(const char* dir_name, const char* fname) {
  size_t n_bytes = snprintf(NULL, 0, "%s/%s", dir_name, fname);
  char* ret = calloc(n_bytes + 2, sizeof(char));
  snprintf(ret, n_bytes + 1, "%s/%s", dir_name, fname);
  return ret;
}


static uint8_t check_magic(char* buf) {
  // Ensure the magic numbers are correct.
  if (memcmp(buf, MAGIC, sizeof(MAGIC)-1) != 0) {
    printf("ERROR: This doesn't look like a KAR archive (MAGIC INVALID)\n");
    free(buf);
    return 1;
  }

  return 0;
}


void archive_append(const char* archive_path, const char* src_path) {
  output_file = fopen(archive_path, "w");
  fprintf(output_file, "%s", MAGIC);


  if (output_file == NULL)
    return;

  DIR* dir = opendir(src_path);

  if (dir == NULL) {
    printf("Could not open directory \"%s\"\n", src_path);
    fclose(output_file);
    exit(1);
  }

  struct dirent* de;
  while ((de = readdir(dir)) != NULL) {
    if (*de->d_name == '.')
      continue;

    if (strlen(de->d_name) >= 195) {
      printf("ERROR: PATH NAME: \"%s\" EXCEEDS FILE PATH LIMIT.\n", de->d_name);
      break;
    }

    /*
     *
     *  It stores the file like this:
     *
     *  FILENAME\FILESIZE\FILE_CONTENTS..more files
     *
     *
     */
    
    char* filename = get_path(src_path, de->d_name);
    FILE* fp = fopen(filename, "r");

    if (fp == NULL) {
      perror("error");
      free(filename);
      break;
    }

    fseek(fp, 0, SEEK_END);
    size_t fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // Read file into buffer.
    char* file_buf = calloc(fsize + 1, sizeof(char));
    fread(file_buf, fsize, sizeof(char), fp);
    fclose(fp);

    fprintf(output_file, "%s\\%x\\%s", de->d_name, fsize, file_buf);
    free(file_buf);
    free(filename);
  }

  closedir(dir);
  fclose(output_file);
}


void extract_archive(const char* archive_path) {
  FILE* fp = fopen(archive_path, "r");

  if (fp == NULL) {
    printf("ERROR: ARCHIVE \"%s\" NOT FOUND!\n", archive_path);
    return;
  }

  fseek(fp, 0, SEEK_END);
  size_t fsize = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  char* buf = calloc(fsize + 3, sizeof(char));
  fread(buf, fsize, sizeof(char), fp);

  if (check_magic(buf) != 0) {
    fclose(fp);
    return;
  }

  char filename_buf[194];
  size_t filename_buf_idx = 0;

  char sizebuf[50];
  size_t size_buf_idx = 0;

  size_t file_size = 0;

  // Parse the file.
  size_t i;

  /*
   *  i is started at sizeof(MAGIC)-1 so we 
   *  can be past the magic numbers.
   */

  for (i = sizeof(MAGIC)-1; i < fsize; ++i) { 
    memset(sizebuf, 0, sizeof(sizebuf));
    memset(filename_buf, 0, sizeof(filename_buf));
    filename_buf_idx = size_buf_idx = 0;

    while (buf[i] != '\\') {
      filename_buf[filename_buf_idx++] = buf[i++];
    }

    ++i;

    // Null terminate the filename.
    filename_buf[filename_buf_idx] = '\0';

    while (buf[i] != '\\' && buf[i] != EOF) {
      sizebuf[size_buf_idx++] = buf[i++];
    } 

    ++i;

    // Null terminate sizebuf.
    sizebuf[size_buf_idx] = '\0';

    // Compute the decimal filesize value.
    file_size = hex2int(sizebuf, size_buf_idx);

    // Write out the extracted archive.
    FILE* fp_out = fopen(filename_buf, "w");

    for (uint32_t fidx = 0; fidx < file_size; ++fidx) {
      fprintf(fp_out, "%c", buf[i++]);
    }

    --i;
    fclose(fp_out);
  }
  fclose(fp);
  free(buf);
}
