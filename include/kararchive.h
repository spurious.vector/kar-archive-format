#ifndef ARCHIVE_H_
#define ARCHIVE_H_


void archive_append(const char* archive_path, const char* src_path);
void extract_archive(const char* archive_path);

#endif
